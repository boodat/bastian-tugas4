import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Splashscreen from "./screens/Splashscreen";
import AuthNavigation from "./screens/AuthNavigation";

const Stack = createNativeStackNavigator();

export default function Routing ({Navigator, route}) {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="SplashScreen" component={Splashscreen}/>
                <Stack.Screen name="AuthNavigation" component={AuthNavigation}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

// export default Routing;