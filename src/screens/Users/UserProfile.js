import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, ScrollView } from "react-native";
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

const UserProfile =({
    navigation,
   route
}) =>{
    return (

        <ScrollView>
        <View>
            <View style={{
                alignItems: 'center',
                paddingVertical: 10,
                backgroundColor: '#fff'
            }}>

                <Image source={require('./../../assets/images/userprofile.png')}
                style={{
                    marginTop: 10,
                    width: 90,
                    height: 90,
                }}/>
                <Text style={{
                    fontSize: 20,
                    color: '#050152',
                    fontWeight: 'bold',
                    marginVertical: 5,
                    textAlign: 'center',
                    fontFamily: 'Montserrat'
                }}>
                    Agil Bani
                </Text>

                <Text
                style={{
                    fontSize: 14,
                    color: '#A8A8A8',
                }}>gilagil@gmail.com</Text>

                <TouchableOpacity onPress={() => navigation.navigate('Edit Profile')}>
                <View 
                style={{
                    backgroundColor: '#F6F8FF',
                    borderRadius: 20,
                    paddingHorizontal: 20,
                    paddingVertical: 2,
                    marginTop: 15,
                    marginBottom: 10,
                }}>
                    <Text
                    style={{
                        fontFamily: 'Montserrat',
                        fontSize: 14,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        color: '#050152',
                    }}>Edit</Text>
                </View>
                </TouchableOpacity>
            </View>

            <View style={{
                backgroundColor: '#fff',
                marginVertical: 20,
                paddingLeft: 80,
                paddingVertical: 10,
                marginHorizontal: 15,
            }}>
                <TouchableOpacity
                style={{
                    marginVertical: 12,
                }}>
                    <Text
                    style={styles.textPress}
                    >About</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={{
                    marginVertical: 12,
                }}>
                    <Text
                    style={styles.textPress}
                    >Terms & Condition</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={{
                    marginVertical: 12,
                }}
                onPress={() => navigation.navigate('FAQ')}>
                    <Text
                    style={styles.textPress}
                    >FAQ</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={{
                    marginVertical: 12,
                }}>
                    <Text
                    style={styles.textPress}
                    >History</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={{
                    marginVertical: 12,
                }}>
                    <Text
                    style={styles.textPress}
                    >Setting</Text>
                </TouchableOpacity>
            </View>

            <View style={{
                alignItems: 'center',
                justifyContent: 'center',
                paddingTop: 15,
                paddingBottom: 15,
                backgroundColor: '#fff',
                marginHorizontal: 15,
                marginBottom: 20,
            }}>
                <TouchableOpacity
                style={{
                    flexDirection: 'row'
                }}>
                    <SimpleLineIcons name="logout" size={25} style={{color: '#BB2427'}}/>
                    <Text
                    style={{
                        marginLeft: 15,
                        color: '#BB2427',
                        fontSize: 18,
                        fontWeight: '600'
                    }}>
                        Log Out
                    </Text>
                </TouchableOpacity>
            </View>
            </View>

            </ScrollView>

            
    )
}

export default UserProfile;

const styles = StyleSheet.create({
    textPress: {
        fontFamily: 'Montserrat',
        fontSize: 20,
        color: 'black',
        fontWeight: '300',
    }
}
)