import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { 
   View, Text, ScrollView, KeyboardAvoidingView, Image, TextInput, TouchableOpacity, StyleSheet, Dimensions
} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-ionicons';


const Tabs = createBottomTabNavigator();

const Home = ({
   navigation,
   route
}) => {
   return(
    <ScrollView>
        <View style={styles.container}>
            <View style={styles.profileContent}>
                <View style={{
                    alignItems: 'baseline',
                }}>
                    <TouchableOpacity>
            <Image
            source={require('../../assets/images/userprofile.png')}
            style={{
                width: 60,
                height: 60,  
                resizeMode: 'contain',
                borderRadius: 10,
                }}/>
                </TouchableOpacity>
                
                <Text style={{color: '#034262' }}>Boodat</Text>
                </View>
                
            <TouchableOpacity
            onPress={() => navigation.navigate('Keranjang')}>
            <Image
            source={require('../../assets/icons/Bag.png')}
            style={{
                width: 23,
                height: 29, 
                resizeMode: 'contain',
                borderRadius: 8,
                marginTop: 20,
                }}/>
                </TouchableOpacity>
           
            </View>

            <View style = {styles.headContent}>
                <Text style={styles.headerText}>Ingin Merawat Dan Perbaiki 
                {'\n'}Sepatumu? Cari Disini </Text>

                <View style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 20,

                }}>
                    
                <TextInput id='Cari' style={{
                        width: '80%',
                        borderRadius: 10,
                        backgroundColor: '#F6F8FF',
                        paddingHorizontal: 10,
                        paddingLeft: 45,
                     }}/>
                     
                     <Image source={require('../../assets/icons/Search.png')}
                     style={{
                        width: 20,
                        height: 30, 
                        resizeMode: 'contain',
                        marginLeft: -530,
                        justifyContent: 'center',
                        }}/>

                        <TouchableOpacity style={{
                           backgroundColor: '#F6F8FF', 
                           padding: 13,
                           borderRadius: 10,
                           width: 'auto',
                        }}>
                            
                        <Image source={require('../../assets/icons/Filter.png')}
                     style={{
                        width: 25,
                        height: 25, 
                        resizeMode: 'contain',
                        backgroundColor: '#F6F8FF',
                        }}/>
                        </TouchableOpacity>
                        </View>
                    </View>
                </View>
            
            <View style={styles.descContent}>
            <View style={{
                justifyContent: 'space-between',
                flex: 3,
                flexDirection: 'row',
                marginHorizontal: 20,
            }}>
                <View style={{
                    backgroundColor: '#FFF',
                    paddingHorizontal: 30,
                    paddingVertical: 20,
                    borderRadius: 10,
                }}>
                <TouchableOpacity style={{
                    borderRadius: 8,
                    alignSelf: 'flex-start',
                    backgroundColor: '#FFDFE0',
                    borderRadius: 30,
                }}>
                    <Image
                    source={require('../../assets/icons/sepatu.png')}
                    style={{
                        width: 50,
                        height: 50, 
                        resizeMode: 'contain',
                        
                        }}/>
                    </TouchableOpacity>

                    <Text style={{
                    fontFamily: 'Montserrat',
                        fontSize: 14,
                        fontWeight: 'bold',
                        textAlign: 'left',
                        color: 'red',
                        }}>Sepatu</Text> 
                    </View>

                    <View style={{
                    backgroundColor: '#FFF',
                    alignSelf: 'flex-start',
                    paddingHorizontal: 30,
                    paddingVertical: 20,
                    borderRadius: 10,
                }}>
                <TouchableOpacity style={{
                    borderRadius: 8,
                    alignSelf: 'flex-start',
                    backgroundColor: '#FFDFE0',
                    borderRadius: 30,
                }}>
                    <Image
                    source={require('../../assets/icons/tas.png')}
                    style={{
                        width: 50,
                        height: 50, 
                        resizeMode: 'contain',
                        borderRadius: 8,
                        alignSelf: 'center',
                        backgroundColor: '#FFDFE0',
                        borderRadius: 30,
                        }}/>
                        </TouchableOpacity>

                        <Text style={{
                        fontFamily: 'Montserrat',
                        fontSize: 14,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        color: 'red',
                        }}>Tas</Text>
                    </View>

                <View style={{
                    backgroundColor: '#FFF',
                    alignSelf: 'flex-start',
                    paddingHorizontal: 30,
                    paddingVertical: 20,
                    borderRadius: 10,
                }}>
                <TouchableOpacity style={{
                    borderRadius: 8,
                    alignSelf: 'flex-start',
                    backgroundColor: '#FFDFE0',
                    borderRadius: 30,
                }}>
                    <Image
                    source={require('../../assets/icons/jaket.png')}
                    style={{
                        width: 50,
                        height: 50, 
                        resizeMode: 'contain',
                        borderRadius: 8,
                        alignSelf: 'flex-end',
                        backgroundColor: '#FFDFE0',
                        borderRadius: 30,
                        }}/>
                        </TouchableOpacity>

                        <Text style={{
                        fontFamily: 'Montserrat',
                        fontSize: 14,
                        fontWeight: 'bold',
                        textAlign: 'right',
                        color: 'red',
                        }}>Jacket</Text>
                    </View>
                    </View>

                <View style={{
                        justifyContent: 'space-between',
                        flex: 2,
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingTop: 20,
                        marginHorizontal: 20,
                    }}>
                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 15,
                    fontWeight: 'bold',
                    color: 'black',
                    alignSelf: 'flex-start'}}>Rekomendasi Terdekat</Text>
                    
                <TouchableOpacity>
                    <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 14,
                    color: 'red',
                    }}>View All</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.content}>
                <View style={styles.produk}>
                <TouchableOpacity onPress={() => navigation.navigate('DetailProduk')}>
                <View style={styles.produkbtn}>
                    <View> 
                        <Image source={require('../../assets/images/toko1.png')}
                        style={{
                            width: 100,
                            height: 130,
                            resizeMode: 'contain',
                            }}/>
                        </View>
                        
                        <View style={{marginLeft: 10,}}> 
                            <Image source={require('../../assets/icons/rating.png')}
                            style={{
                                width: 60,
                                height: 20,
                                resizeMode: 'contain',
                            }}/>

                        <Text style={{
                            color: '#D8D8D8',
                            fontSize: 12,
                        }}>4.3 ratings</Text>

                        <Text style={{
                            color: 'black',
                            fontSize: 14,
                            fontWeight: 'bold',
                            marginVertical: 10,
                        }}>Jack Repair Gejayan</Text>

                        <Text style={{
                            color: '#D8D8D8',
                            fontSize: 12,
                        }}>Jl. Lurus aja bang</Text>
                        <View style={{
                            backgroundColor: '#E64C3C33',
                            borderRadius: 20,
                            alignSelf:'flex-start',
                            marginTop: 10,
                            paddingHorizontal: 10,
                        }}>
                            <Text
                            style={{
                                fontFamily: 'Montserrat',
                                fontSize: 14,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                color: 'red',
                            }}>TUTUP</Text>
                            </View>
                        </View>
                    </View></TouchableOpacity>
                    <View >
                        <TouchableOpacity>
                        <AntDesign name='heart' size={20} style={{color: 'red', marginRight: 20,}} />   
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={styles.content}>
                <View style={styles.produk}>
                <TouchableOpacity onPress={() => navigation.navigate('DetailProduk')}>
                <View style={styles.produkbtn}>
                    <View> 
                        <Image source={require('../../assets/images/toko2.png')}
                        style={{
                            width: 100,
                            height: 130,
                            resizeMode: 'contain',
                            }}/>
                        </View>
                        
                        <View style={{marginLeft: 10,}}> 
                            <Image source={require('../../assets/icons/rating.png')}
                            style={{
                                width: 60,
                                height: 20,
                                resizeMode: 'contain',
                            }}/>

                        <Text style={{
                            color: '#D8D8D8',
                            fontSize: 12,
                        }}>4.3 ratings</Text>

                        <Text style={{
                            color: 'black',
                            fontSize: 14,
                            fontWeight: 'bold',
                            marginVertical: 10,
                        }}>Jack Repair Seturan</Text>

                        <Text style={{
                            color: '#D8D8D8',
                            fontSize: 12,
                        }}>Jl. Lurus aja bang</Text>
                        <View style={{
                            backgroundColor: '#11A84E1F',
                            borderRadius: 20,
                            alignSelf:'flex-start',
                            marginTop: 10,
                            paddingHorizontal: 10,
                        }}>
                            <Text
                            style={{
                                fontFamily: 'Montserrat',
                                fontSize: 14,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                color: '#11A84E',
                            }}>BUKA</Text>
                            </View>
                        </View>
                    </View></TouchableOpacity>
                    <View >
                        <TouchableOpacity>
                        <AntDesign name='hearto' size={20} style={{color: 'grey', marginRight: 20,}} />    
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

    </ScrollView>

    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20,
        
    },
    content: {
        backgroundColor: '#F6F8FF',
    },
    profileContent: {
        flex: 2,
        justifyContent: 'space-between',
        marginHorizontal: 10,
        flexDirection: 'row',

    },
    headContent: {
        justifyContent: 'space-between',
        marginVertical: 10, 
        marginHorizontal: 10,

    }, 
    headerText: {
        fontFamily: 'Montserrat',
        fontSize: 17,
        fontWeight: 'bold',
        color: 'black',
    },
    descContent: {
        flex: 3,
        backgroundColor: '#F6F8FF',
        paddingVertical: 20,
        justifyContent: 'space-between',
    },
    produk: {
        flex: 2,
        flexDirection: 'row',
        backgroundColor: 'white',
        marginHorizontal:20,
        paddingVertical: 10,
        borderRadius: 10,
        marginBottom: 10,
        justifyContent: 'space-between',
    },produkbtn: {
        flexDirection: 'row',
        alignItems: 'flex-start'
    }
})
export default Home;