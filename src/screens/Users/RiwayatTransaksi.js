import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';

import { View, ScrollView, Text, TouchableOpacity, StyleSheet, Image} from "react-native";
import React, {useState} from 'react';

const Transaksi = ({
    navigation,
    route
 }) => {

    
    return(
        <ScrollView>
            <View style={styles.container}>
            <View>
                <TouchableOpacity onPress={() => navigation.navigate('Detail Reservasi')}>
                <View style={styles.content}>
            
                <View style={styles.produkbtn}>
                        
                        <View style={{marginLeft: 10,}}> 
                        
                        <Text style={{
                            color: '#D8D8D8',
                            fontSize: 13,
                            marginBottom: 5,
                        }}>20 Desember 2020  09:00</Text>

                        <Text style={{
                            color: 'black',
                            fontSize: 14,
                        }}>New Balance - Pink Abu - 40</Text>
                        <Text style={{
                            color: '#201F26',
                            fontSize: 13,
                            // fontWeight: 'bold',
                            marginBottom: 10,
                        }}>Cuci Sepatu</Text>
                        
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}>
                            <View style={{
                            flexDirection: 'row',
                        }}>
                        <Text style={{
                            color: '#201F26',
                            fontSize: 13,
                            marginBottom: 10,
                        }}>Kode Reservasi : </Text>
                        <Text style={{
                            color: '#201F26',
                            fontSize: 14,
                            fontWeight: 'bold'
                        }}>CS122001</Text>
                        </View>
                        <View style={{
                            backgroundColor: '#F29C1F29',
                            borderRadius: 20,
                            paddingHorizontal: 15,
                            justifyContent: 'center'
                        }}>
                            <Text
                            style={{
                                fontFamily: 'Montserrat',
                                fontSize: 14,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                color: '#FFC107',
                            }}>Reserved</Text>
                            </View>
                        </View>
                        </View>
                    </View>
            </View> 
            </TouchableOpacity>
            </View>

            {/* <View>
                <TouchableOpacity onPress={() => navigation.navigate('Detail Reservasi 2')}>
                <View style={styles.content}>
            
                <View style={styles.produkbtn}>
                        
                        <View style={{marginLeft: 10,}}> 
                        
                        <Text style={{
                            color: '#D8D8D8',
                            fontSize: 13,
                            marginBottom: 5,
                        }}>20 Desember 2020  12:00</Text>

                        <Text style={{
                            color: 'black',
                            fontSize: 14,
                        }}>New Balance - Pink Abu - 42</Text>
                        <Text style={{
                            color: '#201F26',
                            fontSize: 13,
                            // fontWeight: 'bold',
                            marginBottom: 10,
                        }}>Ganti Sol Sepatu</Text>
                        
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}>
                            <View style={{
                            flexDirection: 'row',
                        }}>
                        <Text style={{
                            color: '#201F26',
                            fontSize: 13,
                            marginBottom: 10,
                        }}>Kode Reservasi : </Text>
                        <Text style={{
                            color: '#201F26',
                            fontSize: 14,
                            fontWeight: 'bold'
                        }}>CS122002</Text>
                        </View>
                        <View style={{
                            backgroundColor: '#11A84E1F',
                            borderRadius: 20,
                            paddingHorizontal: 15,
                            justifyContent: 'center'
                        }}>
                            <Text
                            style={{
                                fontFamily: 'Montserrat',
                                fontSize: 14,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                color: 'green',
                                textAlign: 'center'
                            }}>Belum Reservasi</Text>
                            </View>
                        </View>
                        </View>
                    </View>
            </View> 
            </TouchableOpacity>
            </View> */}
            
            </View>
        </ScrollView>

    )}

    export default Transaksi;

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            marginHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'column',
            height:'100%'
    
            
        },
        content: {
            backgroundColor: 'white',
            paddingHorizontal: 10,
            paddingVertical: 10,
            marginTop: 10,
            borderRadius: 10,
        },

    })