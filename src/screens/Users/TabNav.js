import React from "react";
import { View, Text, ScrollView } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';

import Home from "./UserHome";
import UserProfile from "./UserProfile";
import Keranjang from "../views/Keranjang";
import Transaksi from "./RiwayatTransaksi";

const Tab = createBottomTabNavigator ();
const TabNav = ( ) => {
    return (
        <Tab.Navigator screenOptions={{
            // headerShown: false,
            tabBarShowLabel: false,
            tabBarInactiveTintColor: 'grey',
            tabBarActiveTintColor: 'tomato',
        }}>
            <Tab.Screen name="Home" component={Home} options={{
                
                tabBarIcon: ({color, size}) => (
                    <Ionicons name='home-outline' size={size} color={color} />
                ),
                headerShown: false,
                
            }}/>
            <Tab.Screen name="Transaksi" component={Transaksi} options={{
                tabBarIcon: ({color, size}) => (
                    <MaterialCommunityIcons name='playlist-check' size={size} color={color} />
                ),
                headerTitle: 'Transaksi'
            }}/>
            <Tab.Screen name="Profile" component={UserProfile} options={{
                tabBarIcon: ({color, size}) => (
                    <Octicons name='person' size={size} color={color} />
                ),
                headerShown: false,
                
            }}/>

        </Tab.Navigator>
    )
}

export default TabNav;