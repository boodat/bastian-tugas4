import React, {useEffect, } from "react";
import { Text, View, Image,  } from "react-native";

 const Splashscreen = ({navigation, route}) => {

    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('AuthNavigation')
        },1000)
    }, [])
    return (
        <View style={{
            flex: 1,
            backgroundColor: '#fff',
            justifyContent: 'center',
            alignItems: 'center',}}>
            <Image source={require('../assets/images/splashscreen.png')}
            style={{width: 120, height: 120, resizeMode: 'contain'}}/>
            
        </View>
    )
};

export default Splashscreen;