import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Icon from "react-native-ionicons";

import Login from "./LoginScreen";
import Register from './RegisterScreen';
import Home from './Users/UserHome';
import Transaction from './Users/RiwayatTransaksi';
import DetailProduk from './views/DetailProduk';
import { NavigationContainer } from "@react-navigation/native";
import Pemesanan from "./views/FormPemesanan";
import Keranjang from "./views/Keranjang";
import TabNav from "./Users/TabNav";
import Summary from "./views/Summary";
import Reservasi from "./views/Reservasi";
import EditProfile from "./views/EditProfile";
import Faq from "./views/Faq";
import DetailHistori from "./views/DetailHistori";
import BelumReservasi from "./views/DetailHistori2";
import Checkout from "./views/CheckOut";
import Contoh from "./views/example";
import Promo from "./views/Promos";

const Stack = createNativeStackNavigator();



const AuthNavigation =({}) =>{
    return (
            <Stack.Navigator >
                <Stack.Screen name="Login" component={Login} options={{headerShown: false}}></Stack.Screen>
                <Stack.Screen name="Register" component={Register} options={{headerShown: false}} ></Stack.Screen>
                <Stack.Screen name="Beranda" component={TabNav} options={{headerShown: false}}></Stack.Screen>
                <Stack.Screen name="DetailProduk" component={DetailProduk} options={{headerShown: false}}></Stack.Screen>
                <Stack.Screen name="Form Pemesanan" component={Pemesanan} ></Stack.Screen>
                <Stack.Screen name="Keranjang" component={Keranjang}></Stack.Screen>
                <Stack.Screen name="Summary" component={Summary}></Stack.Screen>
                <Stack.Screen name="Reservasi" component={Reservasi} options={{headerShown: false}}></Stack.Screen>
                <Stack.Screen name="Edit Profile" component={EditProfile}></Stack.Screen>
                <Stack.Screen name="Detail Reservasi" component={DetailHistori} options={{headerTitleStyle: {
                    color: '#fff'
                }}}></Stack.Screen>
                <Stack.Screen name="Detail Reservasi 2" component={BelumReservasi} options={{headerTitleStyle: {
                    color: '#fff'
                }}}></Stack.Screen>
                <Stack.Screen name="Checkout" component={Checkout}></Stack.Screen>
                <Stack.Screen name="FAQ" component={Faq}></Stack.Screen>
                <Stack.Screen name="Kode Promo" component={Promo}></Stack.Screen>
                {/* <Stack.Screen name="Contoh" component={Contoh}></Stack.Screen> */}
            </Stack.Navigator>

           
    )
}

export default AuthNavigation;