import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome6 from 'react-native-vector-icons/FontAwesome6';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';

import { View, ScrollView, Text, TouchableOpacity, StyleSheet, Image} from "react-native";
import React, {useState} from 'react';

const Reservasi = ({
    navigation,
    route
 }) => {

    
    return(
            <View style={styles.container}>
                <View>
                    <TouchableOpacity>
                    <Feather name='x' size={28} color={'black'}/>
                    </TouchableOpacity>
                </View>

                <View style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 3,
                }}>
                    <Text style={{
                        fontSize: 20,
                        fontWeight: 'bold',
                        color: '#11A84E',
                        marginVertical: 30,
                    }}>RESERVASI BERHASIL</Text>

                    <View style={{
                        backgroundColor: '#95e6bc',
                        borderRadius: 50,
                        padding: 36,
                        marginVertical: 20,
                    }}>
                        <FontAwesome6 name='check' size={100} color={'#11A84E'}/>
                    </View>

                    <View >

                    <Text style={{
                        fontSize: 18,
                        color: 'black',
                        marginVertical: 30,
                        marginHorizontal: 50,
                        fontWeight: '360',
                        textAlign: 'center'
                    }}>Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi</Text>
                    </View>
                </View>

                <View >
            <TouchableOpacity
                     style={styles.sbmtBtn}
                     onPress={() => navigation.navigate('Transaksi')}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 16, 
                        fontWeight: 'bold'
                     }}>
                        Selanjutnya
                     </Text>
                  </TouchableOpacity>

                  </View>
                
            </View>

    )
 }

 export default Reservasi;

 const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 17,
        flexDirection: 'column',
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    sbmtBtn: {
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        alignItems: 'center',
        marginHorizontal: 10,
    }

}
 )