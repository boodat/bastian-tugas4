import React from 'react';
import { 
   View, Text, ScrollView, KeyboardAvoidingView, Image, TextInput, TouchableOpacity, StyleSheet, Dimensions
} from 'react-native';

import Octicons from 'react-native-vector-icons/Octicons';

const BelumReservasi = ({
   navigation,
   route
}) => {
   return(

    <ScrollView>
    <View>
        <View style={{
            backgroundColor: '#fff',
            alignItems: 'center',
            paddingVertical: 20,
            marginTop: 10,

        }}>
            <Text style={{
                marginBottom: 20,
                fontSize: 14,
            }}>
                20 Desember 2020   09:00
            </Text>
            <Text style={{
                fontSize: 26,
                color: 'black',
                fontWeight: '700',
                marginBottom: 5,
            }}>
                CS122001
            </Text>
            <Text style={{
                color: 'black',
                fontSize: 16,
                marginBottom: 20,
            }}>
                Kode Reservasi
            </Text>
            <Text style={{
                fontSize: 16,
                paddingHorizontal: 80,
                textAlign: 'center'

            }}>
            Sebutkan Kode Reservasi saat tiba di outlet
            </Text>
        </View>
        <Text style={{
            marginLeft: 10,
            marginTop: 15,
            marginBottom: 10,
            fontSize: 17,
            color: '#201F26'
        }}> Barang </Text>
        <View style={styles.produkbtn}>
                    <View> 
                        <Image source={require('../../assets/images/namapesanan.png')}
                        style={{
                            width: 100,
                            height: 100,
                            resizeMode: 'contain',
                            }}/>
                        </View>
                        
                        <View style={{marginLeft: 10,}}>
                            <Text style={{
                                color: 'black',
                                fontSize: 14,
                                marginVertical: 5,
                            }}>New Balance - Pink Abu - 42</Text>

                            <Text style={{
                                color: 'grey',
                                fontSize: 13,
                                marginVertical: 5,
                            }}>Ganti Sol Sepatu</Text>
                        
                            <Text style={{
                                color: 'grey',
                                fontSize: 13,
                                marginVertical: 5,
                            }}>Note: -</Text>
                        
                        </View>
                        
                    </View>
            <Text style={{
                marginLeft: 10,
                marginTop: 20,
                marginBottom: 10,
                fontSize: 17,
                color: '#201F26'
            }}>Status Pesanan </Text>

            <View style={{
                padding: 10,
                backgroundColor: '#fff',
                flexDirection: 'row',
                alignItems: 'center',
                marginHorizontal: 10,
                borderRadius: 10,
                paddingVertical: 25,
                justifyContent: 'space-between'
            }}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <Octicons name='dot-fill' size={25}
                style={{
                    color: 'green',
                    paddingHorizontal: 20,
                    }} />
                <View>
                    <Text style={{
                        fontSize: 16,
                        color: 'black',

                    }}>Belum Reservasi</Text>
                    <Text>20 Desember 2020</Text>
                </View>
                </View>

                <Text style={{marginRight: 20,}}> 12:00</Text>
                
            </View>
            <TouchableOpacity
                     style={{
                        marginVertical: 30,
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginHorizontal: 10,
                     }}
                     onPress={() => navigation.navigate('Summary')}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 16, 
                        fontWeight: 'bold'
                     }}>
                        Check Out
                     </Text>
                  </TouchableOpacity>
    </View>
    </ScrollView>
   )
}

export default BelumReservasi;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        justifyContent: 'space-between',
        flexDirection: 'column',
        height:'100%',

        
    },
    content: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 10,
        marginTop: 10,
        borderRadius: 10,
    },
    produk: {
        flex: 2,
        flexDirection: 'row',
        backgroundColor: 'white',
        marginHorizontal:20,
        paddingVertical: 10,
        borderRadius: 10,
        marginBottom: 10,
        justifyContent: 'space-between',
    },
    produkbtn: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        padding: 15,
        marginHorizontal: 10,
        borderRadius: 10,
    },

})