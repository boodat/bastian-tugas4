import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome6 from 'react-native-vector-icons/FontAwesome6';

import { View, ScrollView, Text, TouchableOpacity, StyleSheet, Image} from "react-native";
import React, {useState} from 'react';

const Checkout = ({
    navigation,
    route
 }) => {
    const [bayar, setBayar] = useState([
        { key: pilih, checked: false}]);
        const pilih = ['First Check', 'Second Check', 'Third Check', 'Fourth Check',];
        function pilihBayar(selectedBayar){
            if(bayar.includes(selectedBayar)) {
                setBayar(bayar.filter(Bayar => Bayar !== selectedBayar))
                return;
            }
            setBayar(Bayar => Bayar.concat(selectedBayar))
        }
    return(
        <ScrollView>
        <View style={styles.container}>
            <View>
                <View style={styles.content}>
            
                <View style={styles.produkbtn}>
                        
                        <View style={{marginLeft: 10,}}> 
                        
                        <Text style={{
                            color: 'grey',
                            fontSize: 14,
                            marginBottom: 5,
                        }}>Data Customer</Text>

                        <Text style={{
                            color: 'black',
                            fontSize: 13,
                        }}>Agil Bani  (0813763476) {'\n'}Jl. Perumnas, Condong catur, Sleman, Yogyakarta</Text>
                        <Text style={{
                            color: 'black',
                            fontSize: 13,
                            marginVertical: 5,
                        }}>gantengdoang@dipanggang.com</Text>
                        
                        </View>
                    </View>
            </View>

            <View style={styles.content}>
            <View style={styles.produkbtn}>
                <View style={{marginLeft: 10,}}> 
                <Text style={{
                    color: 'grey',
                    fontSize: 14,
                    marginBottom: 5,
                    }}>Alamat Outlet Tujuan</Text>
                    
                     <Text style={{
                        color: 'black',
                        fontSize: 13,
                        }}>Jack Repair - Seturan  (027-343457)
                        {'\n'}Jl. Affandi No 18, Sleman, Yogyakarta</Text>
                        
                </View>
            </View>
            </View>

            <View style={styles.content2}>
                    <Text style={{
                            color: 'grey',
                            fontSize: 16,
                            paddingHorizontal: 10
                        }}>Barang</Text>
            
                <View style={styles.produkbtn}>
                    <View style={{paddingHorizontal: 10}}> 
                        <Image source={require('../../assets/images/namapesanan.png')}
                        style={{
                            width: 90,
                            height: 90,
                            resizeMode: 'contain',
                            }}/>
                        </View>
                        
                        <View style={{marginLeft: 10,}}> 
                            

                        <Text style={{
                            color: 'black',
                            fontSize: 14,
                            // fontWeight: 'bold',
                            marginBottom: 10,
                        }}>New Balance - Pink Abu - 40</Text>

                        <Text style={{
                            color: 'grey',
                            fontSize: 13,
                        }}>Cuci Sepatu</Text>
                        
                        <Text style={{
                            color: 'grey',
                            fontSize: 13,
                        }}>Note: -</Text>
                        
                        </View>
                        
                    </View>

                    
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        paddingHorizontal: 10,
                        marginVertical: 5,
                    }}>
                        <Text style={{
                            fontSize: 15,
                            color: 'black',
                        }}>1 Pasang</Text>
                        <Text style={{
                            fontSize: 16,
                            color: 'black',
                            fontWeight: 'bold'
                        }}>@RP 50.000</Text>
                        </View>
            </View>
            <View style={{
                backgroundColor: '#fff',
                marginVertical: 10,
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 10,
            }}>
            <Text style={{
                    color: 'grey',
                    fontSize: 14,
                    marginBottom: 5,
                    }}>Rincian Pembayaran</Text>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            
                        }}>
                    <Text style={{
                    color: 'black',
                    fontSize: 13,
                    marginBottom: 5,
                    }}>Cuci Sepatu </Text>
                    <Text style={{
                    color: '#FFC107',
                    fontSize: 13,
                    marginBottom: 5,
                    paddingLeft: 20,
                    }}>X1 Pasang</Text>
                    </View>
                    <Text style={{
                    color: 'black',
                    fontSize: 13,
                    marginBottom: 5,
                    }}>RP. 30.000</Text>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                    }}>
                    <Text style={{
                    color: 'black',
                    fontSize: 13,
                    marginBottom: 5,
                    }}>Biaya Antar</Text>
                    <Text style={{
                    color: 'black',
                    fontSize: 13,
                    marginBottom: 5,
                    }}>RP. 3.000</Text>
                    </View>

                    <View style={{
                        marginVertical: 3,
                        backgroundColor: '#EDEDED',
                        padding: 0.6,
                    }}>

                    </View>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                    }}>
                    <Text style={{
                    color: 'black',
                    fontSize: 13,
                    marginBottom: 5,
                    }}>Total </Text>
                    <Text style={{
                    color: '#034262',
                    fontSize: 13,
                    marginBottom: 5,
                    fontWeight: '700'
                    }}>RP. 33.000</Text>
                    </View>
                    
            </View>

            
            <View style={{
                backgroundColor: '#fff',
                marginVertical: 10,
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 10,
            }}>
            <Text style={{
                    color: 'grey',
                    fontSize: 14,
                    marginBottom: 5,
                    }}>Pilih Pembayaran</Text>
                    
    <View style={{flex: 1, backgroundColor: '#fff'}}>
        <ScrollView horizontal= {true}>
            <View style={{
        width: 130,
        height: 90,
        borderColor: '#E1E1E1',
        borderWidth: 1,
        marginRight: 15,
        backgroundColor: 'white',
        borderRadius: 10,
        }}>
            <TouchableOpacity
            onPress={() => pilihBayar('First Check')}>
            {bayar.includes('First Check') && 
            <View style={{flexDirection: 'row',
            marginBottom: -90,
            width: 130,
            height: 90,
            borderColor: '#034262',
            borderWidth: 1,
            backgroundColor: '#03426229',
            borderRadius: 10,
            justifyContent: 'flex-end',
            }}>
                <AntDesign
                name='checkcircle'
                size={20}
                style={{
                    paddingTop: 5,
                    marginRight: 7,
                    color: '#034262',
                    }}/>
                    </View>}
                    <View style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 15,
                    }}>
                    <FontAwesome6
                    name='money-bill-transfer'
                    size={30} 
                    style={{
                        color: 'black'
                        }}/>
                        
                        <Text style={{
                            marginVertical: 5,
                            color: 'black',
                            fontSize: 14,
                        }}>Bank Transfer</Text>
                    </View>
                    </TouchableOpacity>
                    </View>

                    
                    <View style={{
                        width: 130,
                        height: 90,
                        borderColor: '#E1E1E1',
                        borderWidth: 1,
                        marginRight: 15,
                        backgroundColor: 'white',
                        borderRadius: 10,
                    }}>
                        <TouchableOpacity
                        onPress={() => pilihBayar('Second Check')}>
                            {bayar.includes('Second Check') && 
                            <View style={{flexDirection: 'row',
                            marginBottom: -90,
                            width: 130,
                                height: 90,
                                borderColor: '#034262',
                                borderWidth: 1,
                                backgroundColor: '#03426229',
                                borderRadius: 10,
                                justifyContent: 'flex-end',
                                }}>
                    <AntDesign name='checkcircle'
                            size={20}
                            style={{
                                paddingTop: 5,
                                marginRight: 7,
                                color: '#034262',
                            }}/>
                            </View>}
                            <View style={{
                        
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 15,
                    }}>
                        <Image source={require('./../../assets/icons/ovo.png')}
                        style={{
                            width: 60,
                            height: 30,
                            resizeMode: 'contain'
                        }}/>

                        <Text style={{
                            marginVertical: 5,
                            color: 'black',
                            fontSize: 14,
                        }}>OVO</Text>
                    </View>
                    </TouchableOpacity>
                    </View>

                    
                    
                    <View style={{
                        width: 130,
                        height: 90,
                        borderColor: '#E1E1E1',
                        borderWidth: 1,
                        marginRight: 15,
                        backgroundColor: 'white',
                        borderRadius: 10,
                    }}>
                    <TouchableOpacity
                        onPress={() => pilihBayar('Third Check')}>
                            {bayar.includes('Third Check') && 
                            <View style={{flexDirection: 'row',
                            marginBottom: -90,
                            width: 130,
                                height: 90,
                                borderColor: '#034262',
                                borderWidth: 1,
                                backgroundColor: '#03426229',
                                borderRadius: 10,
                                justifyContent: 'flex-end',
                                }}>
                    <AntDesign name='checkcircle'
                            size={20}
                            style={{
                                paddingTop: 5,
                                marginRight: 7,
                                color: '#034262',
                            }}/>
                            </View>}
                            <View style={{
                        
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 15,
                    }}>
                        <AntDesign name='creditcard' size={30} style={{
                            color: 'black'
                        }}/>

                        <Text style={{
                            marginVertical: 5,
                            color: 'black',
                            fontSize: 14,
                        }}>Kartu Kredit</Text>
                    </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>            
      </View>

                    
            </View>
            
            </View>
            <View>
            <TouchableOpacity
                     style={styles.sbmtBtn}
                    //  onPress={() => navigation.navigate('Contoh')}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 16, 
                        fontWeight: 'bold'
                     }}>
                        Pesan Sekarang
                     </Text>
                  </TouchableOpacity>

                  </View>

                  
            </View>

            </ScrollView>
    )
}

export default Checkout;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 10,
        justifyContent: 'space-between',
        flexDirection: 'column',
        height:'100%'    
    },
    content: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginTop: 10,
        borderRadius: 10,
    }, 
    content2: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginTop: 10,
        borderRadius: 10,
    },
    headerText: {
        fontFamily: 'Montserrat',
        fontSize: 17,
        fontWeight: 'bold',
        color: 'black',
    },
    descContent: {
        backgroundColor: '#F6F8FF',
        paddingVertical: 20,
        justifyContent: 'space-between',
    },
    produkbtn: {
        flexDirection: 'row',
        backgroundColor: 'white',
        marginVertical: 5,
    }, 
    sbmtBtn: {
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        alignItems: 'center',
        marginVertical: 20,
    }
})