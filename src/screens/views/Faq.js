import React from "react";
import { View, Text, ScrollView, Touchable, } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


const Faq = ({
    navigation
}) => {
    return (
        <ScrollView style={{
            marginVertical: 20,
        }}>
            <View style={{
                backgroundColor: '#FFF',
                marginHorizontal: 10,
                paddingHorizontal: 20,
                paddingTop: 10,
                paddingBottom: 20,
            }}>
            <View
            style={{
                backgroundColor: '#FFF',
                marginBottom: 20,
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}>
                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    color: 'black'
                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                {/* <View>
                <TouchableOpacity> */}
                <MaterialIcons name="keyboard-arrow-up" size={30}
                style={{color: 'black'}}/>
                {/* </TouchableOpacity>
                </View> */}
                </View>

                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    color: '#595959'
                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
            </View>
            <View style={{
                backgroundColor: '#FFF',
                marginHorizontal: 10,
                marginTop: 20,
                paddingHorizontal: 20,
                paddingTop: 10,
            }}>
            <View
            style={{
                backgroundColor: '#FFF',
                marginBottom: 20,
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}>
                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    color: 'black'
                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                
                <MaterialIcons name="keyboard-arrow-down" size={30}
                style={{color: 'black'}}/>
                </View>
            </View>
            <View style={{
                backgroundColor: '#FFF',
                marginHorizontal: 10,
                marginTop: 20,
                paddingHorizontal: 20,
                paddingTop: 10,
            }}>
            <View
            style={{
                backgroundColor: '#FFF',
                marginBottom: 20,
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}>
                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    color: 'black'
                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>

                <MaterialIcons name="keyboard-arrow-down" size={30}
                style={{color: 'black'}}/>
                </View>
            </View>
            <View style={{
                backgroundColor: '#FFF',
                marginHorizontal: 10,
                marginTop: 20,
                paddingHorizontal: 20,
                paddingTop: 10,
            }}>
            <View
            style={{
                backgroundColor: '#FFF',
                marginBottom: 20,
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}>
                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    color: 'black'
                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>

                <MaterialIcons name="keyboard-arrow-up" size={30}
                style={{color: 'black'}}/>
                </View>
            </View>
            <View style={{
                backgroundColor: '#FFF',
                marginHorizontal: 10,
                marginTop: 20,
                paddingHorizontal: 20,
                paddingTop: 10,
            }}>
            <View
            style={{
                backgroundColor: '#FFF',
                marginBottom: 20,
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}>
                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    color: 'black'
                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>

                <MaterialIcons name="keyboard-arrow-down" size={30}
                style={{color: 'black'}}/>
                </View>
            </View>
            <View style={{
                backgroundColor: '#FFF',
                marginHorizontal: 10,
                marginTop: 20,
                paddingHorizontal: 20,
                paddingTop: 10,
            }}>
            <View
            style={{
                backgroundColor: '#FFF',
                marginBottom: 20,
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}>
                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                    color: 'black'
                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>

                <MaterialIcons name="keyboard-arrow-down" size={30}
                style={{color: 'black'}}/>
                </View>
            </View>
            
        </ScrollView>
    )
}

export default Faq;