import React from 'react';
import { 
   View, Text, ScrollView, KeyboardAvoidingView, Image, TextInput, TouchableOpacity, StyleSheet, Dimensions
} from 'react-native';
const DetailProduk = ({
   navigation,
   route
}) => {
   return(
    <ScrollView>
        
        <View style={styles.container}>
        

        <Image source={require('../../assets/images/tokoprofile.png')}
            style={{
                width: 500,
                height: 500,
                resizeMode: 'cover',
                position: 'relative',
                marginVertical: -150,
             }}/>
             <View style={styles.content}>
                <View>
                <Text style={{
                    fontFamily: 'Montserrat',
                    fontSize: 17,
                    fontWeight: 'bold',
                    color: 'black',
                }}>Jack Repair Seturan</Text>
                <Image source={require('../../assets/icons/rating.png')}
                style={{
                    width: 70,
                    height: 30,
                    resizeMode: 'contain',
                 }}/>
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, alignItems: 'center', marginHorizontal: 5,}}>
                    <Image source={require('../../assets/icons/location.png')}
                    style={{
                        width: 30,
                        height: 30,
                        resizeMode: 'contain',
                        alignSelf: 'flex-start',
                     }}/>
                     <Text style={{
                        width: '65%'
                     }}>Jalan Affandi (Gejayan), No.15, Sleman Yogyakarta, 55384</Text>

                     <TouchableOpacity>
                        <Text style={{color: 'blue', fontSize: 14, fontWeight: 'bold'}}>Lihat Maps</Text>
                     </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', alignItems:'center', }}>
                <View style={{
                            backgroundColor: '#11A84E1F',
                            borderRadius: 20,
                            alignSelf:'flex-start',
                            marginTop: 10,
                            padding: 2,
                            paddingHorizontal: 15,
                        }}>
                            <Text
                            style={{
                                fontFamily: 'Montserrat',
                                fontSize: 16,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                color: '#11A84E',
                            }}>BUKA</Text>
                            
                    </View>
                    <Text style={{
                        color: 'black',
                        fontSize: 16, 
                        fontWeight: 'bold', 
                        paddingVertical: 12, 
                        marginLeft: 20, 
                        }}>09:00 - 21:00    </Text>
                </View>
             </View>

             <View style={styles.descArea}>
                <Text style={{
                    color: 'black', fontSize: 17, fontWeight: 'bold',
                    fontFamily: 'Montserrat',
                    marginBottom: 10,
                }}>Deskripsi</Text>
                <Text style={{
                    fontSize: 15,
                    // textAlign: 'justify',
                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Massa gravida mattis arcu interdum lectus egestas scelerisque.
                    Blandit porttitor diam viverra amet nulla sodales aliquet est.
                    Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec
                    tristique condimentum ornare imperdiet facilisi pretium molestie.</Text>
                    <Text style={{
                    color: 'black', fontSize: 17, fontWeight: 'bold',
                    fontFamily: 'Montserrat',
                    marginVertical: 7,
                }}>Range Biaya</Text>
                <Text style={{
                    fontSize: 17,
                    fontFamily: 'Montserrat',
                    
                }}>Rp 20.000 - 80.000 </Text>
                <TouchableOpacity
                     style={{
                        width: '100%',
                        marginTop: 30,
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center'
                     }}
                     onPress={() => navigation.navigate('Form Pemesanan')}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 16, 
                        fontWeight: 'bold'
                     }}>
                        Repair Disini
                     </Text>
                  </TouchableOpacity>
             </View>
             
            </View>

    </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#d9d9d9',
        // marginTop: -100,
        // padding: 20,
        
    },
    content: {
        backgroundColor: '#FFF',
        paddingHorizontal: 20,
        paddingTop: 20,
        paddingBottom: 10,
        // marginTop: -100,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,

    },
    profileContent: {
        flex: 2,
        justifyContent: 'space-between',
        marginHorizontal: 10,
        flexDirection: 'row',

    },
    headContent: {
        justifyContent: 'space-between',
        marginVertical: 10, 
        marginHorizontal: 10,

    }, 
    headerText: {
        fontFamily: 'Montserrat',
        fontSize: 17,
        fontWeight: 'bold',
        color: 'black',
    },
    descContent: {
        flex: 3,
        backgroundColor: '#F6F8FF',
        paddingVertical: 20,
        justifyContent: 'space-between',
    },
    produk: {
        flex: 2,
        flexDirection: 'row',
        backgroundColor: 'white',
        marginHorizontal:20,
        paddingVertical: 10,
        borderRadius: 10,
        marginBottom: 10,
        justifyContent: 'space-between',
    },
    produkbtn: {
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    descArea: {
        backgroundColor: '#FFF',
        marginTop: 1.5,
        padding: 20,
    }
})
export default DetailProduk;