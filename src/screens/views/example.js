import React, {useState} from 'react';
import { 
   View,
   Text, 
   ScrollView,
   KeyboardAvoidingView, 
   Image, 
   TextInput, 
   TouchableOpacity, 
   StyleSheet, 
   Dimensions
} from 'react-native';

import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome6 from 'react-native-vector-icons/FontAwesome6';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Contoh = ({
   navigation,
   route
}) => {

   const [bayar, setBayar] = useState([
    { key: pilih, checked: false}]);
   const pilih = ['First Check', 'Second Check', 'Third Check', 'Fourth Check',];

   function pilihBayar(selectedBayar){
      if(bayar.includes(selectedBayar)) {
          setBayar(bayar.filter(Bayar => Bayar !== selectedBayar))
          return;
      }

      setBayar(Bayar => Bayar.concat(selectedBayar))

  }
   
   return(
   <View style={{flex: 1, backgroundColor: '#fff'}}>
    <ScrollView horizontal= {true}>
    <View style={{
        width: 130,
        height: 90,
        borderColor: '#E1E1E1',
        borderWidth: 1,
        marginRight: 15,
        backgroundColor: 'white',
        borderRadius: 10,
        }}>
            <TouchableOpacity
            onPress={() => pilihBayar('First Check')}>\
            {bayar.includes('First Check') && 
            <View style={{flexDirection: 'row',
            marginBottom: -90,
            width: 130,
            height: 90,
            borderColor: '#034262',
            borderWidth: 1,
            backgroundColor: '#03426229',
            borderRadius: 10,
            justifyContent: 'flex-end',
            }}>
                <AntDesign
                name='checkcircle'
                size={20}
                style={{
                    paddingTop: 5,
                    marginRight: 7,
                    color: '#034262',
                    }}/>
                    </View>}
                    <View style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 15,
                    }}>
                    <FontAwesome6
                    name='money-bill-transfer'
                    size={30} 
                    style={{
                        color: 'black'
                        }}/>
                        
                        <Text style={{
                            marginVertical: 5,
                            color: 'black',
                            fontSize: 14,
                        }}>Bank Transfer</Text>
                    </View>
                    </TouchableOpacity>
                    </View>

                    
                    <View style={{
                        width: 130,
                        height: 90,
                        borderColor: '#E1E1E1',
                        borderWidth: 1,
                        marginRight: 15,
                        backgroundColor: 'white',
                        borderRadius: 10,
                    }}>
                        <TouchableOpacity
                        onPress={() => pilihBayar('Second Check')}>
                            {bayar.includes('Second Check') && 
                            <View style={{flexDirection: 'row',
                            marginBottom: -90,
                            width: 130,
                                height: 90,
                                borderColor: '#034262',
                                borderWidth: 1,
                                backgroundColor: '#03426229',
                                borderRadius: 10,
                                justifyContent: 'flex-end',
                                }}>
                    <AntDesign name='checkcircle'
                            size={20}
                            style={{
                                paddingTop: 5,
                                marginRight: 7,
                                color: '#034262',
                            }}/>
                            </View>}
                            <View style={{
                        
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 15,
                    }}>
                        <Image source={require('./../../assets/icons/ovo.png')}
                        style={{
                            width: 60,
                            height: 30,
                            resizeMode: 'contain'
                        }}/>

                        <Text style={{
                            marginVertical: 5,
                            color: 'black',
                            fontSize: 14,
                        }}>OVO</Text>
                    </View>
                    </TouchableOpacity>
                    </View>

                    
                    
                    <View style={{
                        width: 130,
                        height: 90,
                        borderColor: '#E1E1E1',
                        borderWidth: 1,
                        marginRight: 15,
                        backgroundColor: 'white',
                        borderRadius: 10,
                    }}>
                    <TouchableOpacity
                        onPress={() => pilihBayar('Third Check')}>
                            {bayar.includes('Third Check') && 
                            <View style={{flexDirection: 'row',
                            marginBottom: -90,
                            width: 130,
                                height: 90,
                                borderColor: '#034262',
                                borderWidth: 1,
                                backgroundColor: '#03426229',
                                borderRadius: 10,
                                justifyContent: 'flex-end',
                                }}>
                    <AntDesign name='checkcircle'
                            size={20}
                            style={{
                                paddingTop: 5,
                                marginRight: 7,
                                color: '#034262',
                            }}/>
                            </View>}
                            <View style={{
                        
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 15,
                    }}>
                        <AntDesign name='creditcard' size={30} style={{
                            color: 'black'
                        }}/>

                        <Text style={{
                            marginVertical: 5,
                            color: 'black',
                            fontSize: 14,
                        }}>Kartu Kredit</Text>
                    </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>            
      </View>

      

   )

}

export default Contoh;

const styles = StyleSheet.create({
    container: {
        padding: 20,

    },

    textInput: {
        marginTop: 15,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 10,
    },
    inputLabel: {
        color: '#BB2427',
        fontWeight: 'bold',
        marginTop: 20,
    },
    checkbox: {
      marginHorizontal: 20, 
    },
    cbButton: {
      width: 25,
      height: 25,
      borderWidth: 1,
      borderColor: 'grey',
      marginRight: 10,
      borderRadius: 7,
    },
    cbIcon: {
      color: 'green', alignSelf: 'center',
    }
})