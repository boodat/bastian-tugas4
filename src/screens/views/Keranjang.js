
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';

import { View, ScrollView, Text, TouchableOpacity, StyleSheet, Image} from "react-native";
import React, {useState} from 'react';

const Keranjang = ({
    navigation,
    route
 }) => {

    
    return(
        // <ScrollView>
        <View style={styles.container}>
            <View style={styles.content}>
            
                <View style={styles.produkbtn}>
                    <View> 
                        <Image source={require('../../assets/images/namapesanan.png')}
                        style={{
                            width: 100,
                            height: 100,
                            resizeMode: 'contain',
                            }}/>
                        </View>
                        
                        <View style={{marginLeft: 10,}}>
                            <Text style={{
                                color: 'black',
                                fontSize: 14,
                                // fontWeight: 'bold',
                                marginVertical: 10,
                            }}>New Balance - Pink Abu - 40</Text>

                            <Text style={{
                                color: '#D8D8D8',
                                fontSize: 13,
                            }}>Cuci Sepatu</Text>
                        
                            <Text style={{
                                color: '#D8D8D8',
                                fontSize: 13,
                            }}>Note: -</Text>
                        
                        </View>
                    </View>

                    
                    
            </View>
            <View style={{flex: 3, }}>
                    <TouchableOpacity
                    style={{
                        marginVertical: 20,
                        flexDirection: 'row',
                        alignItems: 'center',
                        alignSelf: 'center',
                        }}
                        onPress={() => navigation.navigate('Form Pemesanan')}>
                        <Feather name='plus-square' size={30}
                        style={{color: '#BB2427', marginRight: 5,}}/>
                        <Text style={{fontSize: 17, color: '#BB2427', fontWeight: 'bold'}}>Tambah Barang</Text>
                    
                        </TouchableOpacity>
                    </View>
                    
            <View style={{flex: 1, }}>
            <TouchableOpacity
                     style={styles.sbmtBtn}
                     onPress={() => navigation.navigate('Summary')}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 16, 
                        fontWeight: 'bold'
                     }}>
                        Selanjutnya
                     </Text>
                  </TouchableOpacity>

                  </View>
            </View>

            // </ScrollView>
    )
}

export default Keranjang;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        // justifyContent: 'space-between',
        flexDirection: 'column'

        
    },
    content: {
        backgroundColor: 'white',
        padding: 20,
        marginTop: 10,
        borderRadius: 10,
        height: 'auto',
        width: 'auto'
        
    },


    headContent: {
        justifyContent: 'space-between',
        marginVertical: 10, 
        marginHorizontal: 10,

    }, 
    headerText: {
        fontFamily: 'Montserrat',
        fontSize: 17,
        fontWeight: 'bold',
        color: 'black',
    },
    descContent: {
        flex: 3,
        backgroundColor: '#F6F8FF',
        paddingVertical: 20,
        justifyContent: 'space-between',
    },
    produk: {
        
        flexDirection: 'row',
        backgroundColor: 'white',
        paddingVertical: 10,
        borderRadius: 10,
        marginBottom: 10,
        // justifyContent: 'space-between',
    },
    produkbtn: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'flex-start'
    }, 
    sbmtBtn: {
        width: '100%',
                        marginTop: 30,
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center'
    }
})