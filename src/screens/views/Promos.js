import React from "react";
import { 
    View,
    Text, 
    ScrollView,
    KeyboardAvoidingView, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    StyleSheet, 
    Dimensions
 } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

const Promo =({
    navigation,
   route
}) =>{
    return (
        <ScrollView>
            <View style={{
                marginTop: 10,
                marginBottom: 20,
            }}>
                <View style={{
                    backgroundColor: '#fff',
                    paddingHorizontal: 15,
                    paddingVertical: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                    <TextInput placeholder="Masukkan Kode Promo" style={{
                        width: '60%',
                        borderRadius: 8,
                        backgroundColor: '#F6F8FF',
                        paddingHorizontal: 10,
                    }}/>
                    <TouchableOpacity style={{
                        width: '35%',
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center'
                     }}
                     onPress={() => navigation.navigate('Form Pemesanan')}>
                        <Text style={{
                            fontWeight: 'bold',
                            color: '#fff'
                        }}>GUNAKAN</Text>
                    </TouchableOpacity>
                </View>

                <View style={{
                    backgroundColor: '#fff',
                    marginTop: 8,
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginHorizontal: 20,
                    borderRadius: 15,
                    
                }}>
                    <View style={{
                        backgroundColor: '#FFDFE0',
                        padding: 10,
                        paddingVertical: 30,
                        borderRadius: 100,
                        width: 110,
                        height: 110,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'crimson',
                            marginLeft: 1,
                            fontFamily: 'Montserrat',
                            // fontVariant: 
                        }}>30rb</Text>
                        <Text style={{
                            marginTop: -49,
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'black'
                        }}>30rb</Text>
                    </View>
                    <View style={{
                        marginLeft: 20,
                        width: '60%'
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontWeight: '800',
                            color: 'black',
                            width: 'auto'
                        }}>
                        Promo Cashback Hingga 30rb
                        </Text>
                        <Text>09 s/d 15 Maret 2021</Text>
                        <TouchableOpacity>
                            <Text style={{
                                color: '#034262',
                                alignSelf: 'flex-end',
                                marginTop: 16,
                            }}>Pakai Kupon</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    backgroundColor: '#fff',
                    marginTop: 8,
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginHorizontal: 20,
                    borderRadius: 15,
                }}>
                    <View style={{
                        backgroundColor: '#FFDFE0',
                        padding: 15,
                        paddingVertical: 30,
                        borderRadius: 100,
                        width: 110,
                        height: 110,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'crimson',
                            marginLeft: 1,
                            fontFamily: 'Montserrat',
                        }}>50%</Text>
                        <Text style={{
                            marginTop: -49,
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'black'
                        }}>50%</Text>
                    </View>
                    <View style={{
                        marginLeft: 20,
                        width: '60%'
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontWeight: '800',
                            color: 'black',
                            width: 'auto'
                        }}>
                        Promo Discount Cuci Sepatu 50 % 
                        </Text>
                        <Text>09 s/d 15 Maret 2021</Text>
                        <TouchableOpacity>
                            <Text style={{
                                color: '#034262',
                                alignSelf: 'flex-end',
                                marginTop: 16,
                            }}>Pakai Kupon</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    backgroundColor: '#fff',
                    marginTop: 8,
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginHorizontal: 20,
                    borderRadius: 15,
                }}>
                    <View style={{
                        backgroundColor: '#FFDFE0',
                        padding: 15,
                        paddingVertical: 30,
                        borderRadius: 100,
                        width: 110,
                        height: 110,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'crimson',
                            marginLeft: 1,
                            fontFamily: 'Montserrat',
                        }}>50%</Text>
                        <Text style={{
                            marginTop: -49,
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'black'
                        }}>50%</Text>
                    </View>
                    <View style={{
                        marginLeft: 20,
                        width: '60%'
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontWeight: '800',
                            color: 'black',
                            width: 'auto'
                        }}>
                        Promo Discount 50 %  
                        </Text>
                        <Text>09 s/d 15 Maret 2021</Text>
                        <TouchableOpacity>
                            <Text style={{
                                color: '#034262',
                                alignSelf: 'flex-end',
                                marginTop: 16,
                            }}>Pakai Kupon</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    backgroundColor: '#fff',
                    marginTop: 8,
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginHorizontal: 20,
                    borderRadius: 15,
                }}>
                    <View style={{
                        backgroundColor: '#FFDFE0',
                        padding: 15,
                        paddingVertical: 30,
                        borderRadius: 100,
                        width: 110,
                        height: 110,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'crimson',
                            marginLeft: 1,
                            fontFamily: 'Montserrat',
                        }}>50%</Text>
                        <Text style={{
                            marginTop: -49,
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'black'
                        }}>50%</Text>
                    </View>
                    <View style={{
                        marginLeft: 20,
                        width: '60%'
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontWeight: '800',
                            color: 'black',
                            width: 'auto'
                        }}>
                        Promo Discount 50 %  
                        </Text>
                        <Text>09 s/d 15 Maret 2021</Text>
                        <TouchableOpacity>
                            <Text style={{
                                color: '#034262',
                                alignSelf: 'flex-end',
                                marginTop: 16,
                            }}>Pakai Kupon</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    backgroundColor: '#fff',
                    marginTop: 8,
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginHorizontal: 20,
                    borderRadius: 15,
                }}>
                    <View style={{
                        backgroundColor: '#FFDFE0',
                        padding: 15,
                        paddingVertical: 30,
                        borderRadius: 100,
                        width: 110,
                        height: 110,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'crimson',
                            marginLeft: 1,
                            fontFamily: 'Montserrat',
                        }}>50%</Text>
                        <Text style={{
                            marginTop: -49,
                            fontSize: 36,
                            fontWeight: '800',
                            color: 'black'
                        }}>50%</Text>
                    </View>
                    <View style={{
                        marginLeft: 20,
                        width: '60%'
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontWeight: '800',
                            color: 'black',
                            width: 'auto'
                        }}>
                        Promo Discount 50 %  
                        </Text>
                        <Text>09 s/d 15 Maret 2021</Text>
                        <TouchableOpacity>
                            <Text style={{
                                color: '#034262',
                                alignSelf: 'flex-end',
                                marginTop: 16,
                            }}>Pakai Kupon</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </ScrollView>

    )
}

export default Promo;