import React, {useState} from 'react';
import { 
   View,
   Text, 
   ScrollView,
   KeyboardAvoidingView, 
   Image, 
   TextInput, 
   TouchableOpacity, 
   StyleSheet, 
   Dimensions
} from 'react-native';

import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Pemesanan = ({
   navigation,
   route
}) => {

   const [layanans, setLayanan] = useState([]);
   const pilih = ['Ganti Sol Sepatu','Jahit Sepatu', 'Repaint Sepatu', 'Cuci Sepatu'];

   function pilihLayanan(selectedLayanan){
      if(layanans.includes(selectedLayanan)) {
          setLayanan(layanans.filter(Layanan => Layanan !== selectedLayanan))
          return;
      }

      setLayanan(Layanan => Layanan.concat(selectedLayanan))

  }
   
   return(
      <View style={{flex: 1, backgroundColor: '#fff'}}>
         <ScrollView 
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingBottom: 10}}
         >
            
            <KeyboardAvoidingView 
               behavior='padding' 
               enabled
               keyboardVerticalOffset={-500}
            >
               
               <View style={styles.container} >
               <Text style={styles.inputLabel}>
                     Merek
                  </Text> 
                  <TextInput
                     placeholder='Masukkan Email' 
                     style={styles.textInput}
                     keyboardType="email-address" 
                  />
                  <Text style={styles.inputLabel}>
                     Warna
                  </Text> 
                  <TextInput 
                     placeholder='Masukkan Email' 
                     style={styles.textInput}
                     keyboardType="email-address" 
                  />
                  <Text style={styles.inputLabel}>
                     Ukuran
                  </Text> 
                  <TextInput
                     placeholder='Masukkan Email' 
                     style={styles.textInput}
                     keyboardType="email-address" 
                  />
                  <Text style={styles.inputLabel}>
                     Photo
                  </Text>
                  <View style={{
                    marginTop: 10,
                    borderWidth: 1,
                    borderColor: '#BB2427',
                    padding: 10,
                    borderRadius: 10,
                    marginEnd: 'auto',
                    
                  }}>
                    <TouchableOpacity style={{
                        alignSelf: 'center'
                    }}
                  //   onPress={() => navigation.navigate('Keranjang')}
                    >
                        <Image source={require('../../assets/icons/Camera.png')}
                        style={{
                            width: 25,
                            height: 25,
                            resizeMode: 'contain',
                            alignSelf: 'center'
                        }}
                        />
                        <Text style={{
                            color: '#BB2427',
                            fontSize: 13,
                            marginTop: 8,
                            alignSelf: 'center'
                        }}>
                     Add Photo
                  </Text>
                    </TouchableOpacity>
                  
            </View>
               </View> 
               <View style={styles.checkbox}>
                {pilih.map(option => (
                    <View key={option} style={{
                        flexDirection: 'row',
                        marginVertical: 6,
                    }}>
                        <TouchableOpacity style={styles.cbButton}
                        onPress={() => pilihLayanan(option)}>
                            {layanans.includes(option) && 
                            <MaterialCommunityIcons name='check-bold'
                            size={23}
                            style={styles.cbIcon} />
                            }
                        </TouchableOpacity>
                        <Text>{option}</Text>
                    </View>
                ))}
                
            </View> 
            <View style={{
               marginHorizontal: 20,
            }}>
            <Text style={styles.inputLabel}>
                     Catatan
                  </Text> 
                <TextInput 
                     placeholder='Cth: Ganti sol dengan warna ...' 
                     style={{
                        textAlignVertical: 'top',
                        marginTop: 10,
                        height: 100,
                        borderRadius: 8,
                        backgroundColor: '#F6F8FF',
                        paddingHorizontal: 10,
                        
                     }}
                        multiline={true}
                     keyboardType="email-address" 
                  />
                  <Text style={styles.inputLabel}>
                     Kupon Promo
                  </Text>
                  <TouchableOpacity onPress={() => navigation.navigate('Kode Promo')}>
                     <View style={{
                        marginTop: 10,
                        paddingVertical: 6,
                        paddingHorizontal: 15,
                        borderRadius: 7,
                        borderColor: 'red',
                        borderWidth: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent:'space-between'
                     }}>
                        <Text style={{
                           fontSize: 13,
                        }}>Pilih Kupon Promo</Text>
                        <Entypo name='chevron-small-right' size={25}
                        style={{color: 'red'}}/>
                     </View>
                  </TouchableOpacity>
            
            <TouchableOpacity
                     style={{
                        width: '100%',
                        marginTop: 30,
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center'
                     }}
                     onPress={() => navigation.navigate('Keranjang')}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 16, 
                        fontWeight: 'bold'
                     }}>
                        Masukkan Keranjang
                     </Text>
                  </TouchableOpacity>             
                  </View>
            </KeyboardAvoidingView>
            </ScrollView>
      </View>
   )
}
export default Pemesanan;

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,

    },

    textInput: {
        marginTop: 10,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 10,
    },
    inputLabel: {
        color: '#BB2427',
        fontWeight: 'bold',
        marginTop: 15,
    },
    checkbox: {
      marginTop: 15,
      marginHorizontal: 20,
    },
    cbButton: {
      width: 25,
      height: 25,
      borderWidth: 1,
      borderColor: 'grey',
      marginRight: 10,
      borderRadius: 7,
    },
    cbIcon: {
      color: 'green', alignSelf: 'center',
    }
})