
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';

import { View, ScrollView, Text, TouchableOpacity, StyleSheet, Image} from "react-native";
import React, {useState} from 'react';

const Summary = ({
    navigation,
    route
 }) => {

    
    return(
        // <ScrollView>
        <View style={styles.container}>
            <View>
                <View style={styles.content}>
            
                <View style={styles.produkbtn}>
                        
                        <View style={{marginLeft: 10,}}> 
                        
                        <Text style={{
                            color: 'grey',
                            fontSize: 15,
                            marginBottom: 5,
                        }}>Data Customer</Text>

                        <Text style={{
                            color: 'black',
                            fontSize: 14,
                        }}>Agil Bani  (0813763476) {'\n'} Jl. Perumnas, Condong catur, Sleman, Yogyakarta</Text>
                        <Text style={{
                            color: 'black',
                            fontSize: 14,
                            // fontWeight: 'bold',
                            marginVertical: 5,
                        }}>gantengdoang@dipanggang.com</Text>
                        
                        </View>
                    </View>
            </View>

            <View style={styles.content}>
            <View style={styles.produkbtn}>
                <View style={{marginLeft: 10,}}> 
                <Text style={{
                    color: 'grey',
                    fontSize: 16,
                    marginBottom: 5,
                    }}>Alamat Outlet Tujuan</Text>
                    
                     <Text style={{
                        color: 'black',
                        fontSize: 15,
                        }}>Jack Repair - Seturan  (027-343457)
                        {'\n'}Jl. Affandi No 18, Sleman, Yogyakarta</Text>
                        
                </View>
            </View>
            </View>

            <View style={styles.content2}>
                    <Text style={{
                            color: 'grey',
                            fontSize: 16,
                            paddingHorizontal: 10
                        }}>Barang</Text>
            
                <View style={styles.produkbtn}>
                    <View style={{paddingHorizontal: 10}}> 
                        <Image source={require('../../assets/images/namapesanan.png')}
                        style={{
                            width: 90,
                            height: 90,
                            resizeMode: 'contain',
                            }}/>
                        </View>
                        
                        <View style={{marginLeft: 10,}}> 
                            

                        <Text style={{
                            color: 'black',
                            fontSize: 14,
                            marginBottom: 10,
                        }}>New Balance - Pink Abu - 40</Text>

                        <Text style={{
                            color: 'grey',
                            fontSize: 13,
                        }}>Cuci Sepatu</Text>
                        
                        <Text style={{
                            color: 'grey',
                            fontSize: 13,
                        }}>Note: -</Text>
                        
                        </View>
                    </View>

                    
                    
            </View>


            </View>
            

            
                    
                    
            <View>
            <TouchableOpacity
                     style={styles.sbmtBtn}
                     onPress={() => navigation.navigate('Reservasi')}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 16, 
                        fontWeight: 'bold'
                     }}>
                        Reservasi Sekarang
                     </Text>
                  </TouchableOpacity>

                  </View>
            </View>

            // </ScrollView>
    )
}

export default Summary;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        justifyContent: 'space-between',
        flexDirection: 'column',
        // width: '100%',
        height:'100%'

        
    },
    content: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 5,
        // flex: 1.2,
        marginTop: 10,
        borderRadius: 10,
    }, 
    content2: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 5,
        // flex: 1.5,
        marginTop: 10,
        borderRadius: 10,
    },
    headerText: {
        fontFamily: 'Montserrat',
        fontSize: 17,
        fontWeight: 'bold',
        color: 'black',
    },
    descContent: {
        // flex: 3,
        backgroundColor: '#F6F8FF',
        paddingVertical: 20,
        justifyContent: 'space-between',
    },
    produkbtn: {
        flexDirection: 'row',
        backgroundColor: 'white',
        marginVertical: 5,
    }, 
    sbmtBtn: {
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        alignItems: 'center',
        marginVertical: 20,
    }
})