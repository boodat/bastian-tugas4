import React from "react";
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet, ScrollView } from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const EditProfile =({
    navigation,
   route
}) =>{
    return (
        <View style={{
            flex: 1,
            justifyContent: 'space-between',
            backgroundColor: '#fff',
            flexDirection: 'column'
        }}>
        <View style={{
            flex: 4,
        }}>
            <View style={{
                alignItems: 'center',
                paddingVertical: 10,
                backgroundColor: '#fff',
               
            }}>

                <Image source={require('./../../assets/images/userprofile.png')}
                style={{
                    width: 90,
                    height: 90,
                }}/>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}}>
                    <MaterialCommunityIcons name="pencil" size={25}
                    style={{color: '#3A4BE0' }} />
                <Text style={{
                    fontSize: 20,
                    color: '#3A4BE0',
                    marginVertical: 10,
                    paddingLeft: 10,
                    textAlign: 'center',
                    fontFamily: 'Montserrat'
                }}>
                    Edit Foto
                </Text>
                </TouchableOpacity>
            </View>

            <View style={styles.container} >
               <Text style={styles.inputLabel}>
                     Nama
                  </Text> 
                  <TextInput
                     placeholder='Masukkan Nama' 
                     style={styles.textInput}
                     
                  />
                  <Text style={styles.inputLabel}>
                     Email
                  </Text> 
                  <TextInput 
                     placeholder='Masukkan Email' 
                     style={styles.textInput}
                     keyboardType="email-address" 
                  />
                  <Text style={styles.inputLabel}>
                     No. HP
                  </Text> 
                  <TextInput
                     placeholder='Masukkan No Hp' 
                     style={styles.textInput}
                  />
               </View>

               
            </View>

            <View style={{
                flex: 0.5,
            }}>
               <TouchableOpacity
                     style={{
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginHorizontal: 20,
                     }}
                     onPress={() => navigation.navigate('Profile')}
                  >
                     <Text style={{
                        color: '#fff', 
                        fontSize: 18, 
                        fontWeight: 'bold'
                     }}>
                        Simpan
                     </Text>
                  </TouchableOpacity>
                  </View>
            </View>



            
    )
}

export default EditProfile;

const styles = StyleSheet.create({
    textPress: {
        fontFamily: 'Montserrat',
        fontSize: 20,
        color: 'black',
        fontWeight: '300',
    },
    container: {
        paddingHorizontal: 20,
        paddingVertical: 15,
        backgroundColor: '#fff',
    },

    textInput: {
        marginTop: 15,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 10,
    },
    inputLabel: {
        color: '#BB2427',
        fontWeight: 'bold',
        marginTop: 20,
    },
}
)